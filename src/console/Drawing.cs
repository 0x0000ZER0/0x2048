using System;

namespace Console2048
{
    internal static class Drawing
    {
        internal static void ClearScreen()
        {
            Console.Clear();
        }
        internal static void DrawMatrix(this ushort[,] matrix, uint size)
        {
            for (var x = 0u; x < size; ++x)
            {
                for (var y = 0u; y < size; ++y)
                    $"{matrix[y, x]}  ".Print();

                NewLine();
            }
        }

        private static void Print(this string toPrint, uint preNewLines = 0, uint postNewLines = 0)
        {
            for (int i = 0; i < preNewLines; ++i)
                NewLine();

            Console.Write(toPrint);
            
            for (int i = 0; i < postNewLines; ++i)
                NewLine();
        }
        private static void NewLine()
        {
            Console.WriteLine();
        }
    }
}
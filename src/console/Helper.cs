using System;
using System.Linq;
using System.Collections.Generic;

namespace Console2048
{
    internal static class Helper
    {
        internal static void GenerateRandomMatrix(this ushort[,] matrix, ushort size)
        {
            var spots = new List<Cell>();

            for (ushort x = 0; x < size; ++x)
                for (ushort y = 0; y < size; ++y)
                    if (matrix[x, y] == 0)
                        spots.Add(new Cell(x, y));

            var randomCells = spots.GetRandomCells(2);

            for (var i = 0; i < randomCells.Count; i++)
            {
                var n = GetRandomNumber(1,3);
                n = n % 2 != 0 ? n + 1 : n;
                
                var cell = randomCells[i];
                matrix[cell.X, cell.Y] = (ushort)n;
            }
        }
        private static IList<Cell> GetRandomCells(this IList<Cell> cells, ushort count)
        {
            var results = new List<Cell>();

            for (var i = 0; i < count; ++i)
            {
                var rndIndex = GetRandomNumber(0, cells.Count);   
                results.Add(cells[rndIndex]);
                cells.RemoveAt(rndIndex);
            }

            return results;
        }
        private static int GetRandomNumber(int start, int end)
        {
            var random = new Random();

            return random.Next(start, end);
        }
    }
}
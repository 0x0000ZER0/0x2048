﻿namespace Console2048
{
    public sealed class Program
    {
        private static void Main(string[] args)
        {
            var game = new Game();

            game.Start();
        }
    }
}

namespace Console2048
{
    internal readonly struct Cell
    {
        internal ushort X { get; }
        internal ushort Y { get; }

        internal Cell(ushort x, ushort y)
        {
            X = x;
            Y = y;
        }
    }
}
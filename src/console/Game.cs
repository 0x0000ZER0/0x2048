using System;

namespace Console2048
{
    internal sealed class Game
    {
        private readonly ushort[,] _matrix;
        private readonly ushort _size;

        internal Game(ushort size = 4)
        {
            _size = size > ushort.MaxValue ? ushort.MaxValue : size;
            _matrix = new ushort[_size, _size];
        }

        internal void Start()
        {
            Draw();

            Keyboard.Handle(key => 
            {
                Drawing.ClearScreen();

                switch (key)
                {
                    case ConsoleKey.UpArrow:
                        MoveUp();
                        break;
                    case ConsoleKey.LeftArrow:
                        MoveLeft();
                        break;
                    case ConsoleKey.DownArrow:
                        MoveDown();
                        break;
                    case ConsoleKey.RightArrow:
                        MoveRight();
                        break;
                }
                
                Draw();
            });
        }
        private void MoveUp()
        {
            _matrix.ReverseVeritcally(_size);
            _matrix.SlideVertically(_size);
            _matrix.ReverseVeritcally(_size);
        }
        private void MoveLeft()
        {
            _matrix.ReverseHorizontally(_size);
            _matrix.SlideHorizontally(_size);
            _matrix.ReverseHorizontally(_size);
        }
        private void MoveDown()
        {
            _matrix.SlideVertically(_size);
        }
        private void MoveRight()
        {
            _matrix.SlideHorizontally(_size);
        }
        private void Draw()
        {
            _matrix.GenerateRandomMatrix(_size);
            _matrix.DrawMatrix(_size);
        }
    }
}
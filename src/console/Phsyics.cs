namespace Console2048
{
    internal static class Physics
    {
        internal static void ReverseHorizontally(this ushort[,] matrix, ushort size)
        {
            for (var y = 0u; y < size; ++y)
                matrix.ReverseRow(size, y);
        }
        private static void ReverseRow(this ushort[,] matrix, ushort size, uint y)
        {
            for (var x = 0u; x < size / 2; ++x)
            {
                var temp = matrix[x, y];
                matrix[x, y] = matrix[size - x - 1, y];
                matrix[size - x - 1, y] = temp;
            }
        }
        internal static void ReverseVeritcally(this ushort[,] matrix, ushort size)
        {
            for (var x = 0u; x < size; ++x)
                matrix.ReverseCol(size, x);
        }
        private static void ReverseCol(this ushort[,] matrix, ushort size, uint x)
        {
            for (var y = 0u; y < size / 2; ++y)
            {
                var temp = matrix[x, y];
                matrix[x, y] = matrix[x, size - y - 1];
                matrix[x, size - y - 1] = temp;
            }
        }
         internal static void SlideHorizontally(this ushort[,] matrix, ushort size)
        {
            for (var y = 0u; y < size; ++y)
                matrix.SlideRow(size, y);
        }
        private static void SlideRow(this ushort[,] matrix, ushort size, uint y)
        {
            var row = new ushort[size];
            for (var x = 0u; x < size; ++x)
                row[x] = matrix[x, y];

            row.Slide();
            row.Combine();
            row.Slide();

            for (var x = 0u; x < size; ++x)
                matrix[x, y] = row[x];
        }
        internal static void SlideVertically(this ushort[,] matrix, ushort size)
        {
            for (var x = 0u; x < size; ++x)
                matrix.SlideCol(size, x);
        }
        private static void SlideCol(this ushort[,] matrix, ushort size, uint x)
        {
            var col = new ushort[size];
            for (var y = 0u; y < size; ++y)
                col[y] = matrix[x, y];

            col.Slide();        
            col.Combine();
            col.Slide();        

            for (var y = 0u; y < size; ++y)
                matrix[x, y] = col[y];
        }
        private static void Slide(this ushort[] arr)
        {
            var size = arr.Length;

            for (var i = size - 1; i >= 0u; --i)
            {
                if (arr[i] == 0)
                    continue;

                for (var k = size - 1; k >= 0u; --k)
                    if (k == i)
                        break;
                    else if (arr[k] == 0)
                    {
                        arr[k] = arr[i];
                        arr[i] = 0;
                        break;
                    }
            }
        }
        private static void Combine(this ushort[] arr)
        {
            var size = arr.Length;

            for (var i = 0u; i < size - 1; ++i)
                if (arr[i] == arr[i + 1])
                {
                    arr[i] = 0;
                    arr[i + 1] *= 2;
                }
        }
    }
}
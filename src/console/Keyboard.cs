using System;

namespace Console2048
{
    internal static class Keyboard
    {
        internal static void Handle(Action<ConsoleKey> method)
        {
            var exit = false;

            while(!exit)
            {
                var info = Console.ReadKey();
                method(info.Key);
                
                exit = info.Key != ConsoleKey.Escape ? false : true;
            }
        }
    }
}
//definiton of Game class. Is used to manage the game.
class Game{
    // custom constructor: takes 2 parameters
    // param->size: defines the length of the matrix
    // param->value: sets the matrix values with given number
    constructor(size = 4, value = 0){
        this.size = size;
        this.value = value;
        this.matrix = Helper.newMatrix(size, value);
        this.graphic = new Graphic(this);        
    }
    //starts the game with 2 random numbers. a.k.a entry point
    start(){
        this.generate();
        this.generate();
        
        console.table(game.matrix);
        this.graphic.drawGrid();
    }
    //generates a new random field in the matrix 
    generate(){
        let options = [];
        for (let c = 0; c < this.size; c++)
            for (let r = 0; r < this.size; r++)
                if (this.matrix[c][r] === 0) 
                    options.push(new Cell(c, r));

        if (options.length <= 0)
            return;

        const randCellIndex = Helper.getRandomNumber(0, options.length - 1);
        const randCell = options[randCellIndex];
        const rand = Helper.getRandomNumber(0, 1);
        
        this.matrix[randCell.col][randCell.row] = rand > 0 ? 2 : 4;
    }
    //slides a matrix column values to the right: takes 1 parameter
    //param->col: given column of the matrix
    slide(col){
        let arr = [];
        for (let i = 0; i < col.length; i++)
            if (col[i] !== 0)
                arr.push(col[i]);

        const size = this.size - arr.length;
        
        let zeros = [];
        for (let i = 0; i < size; i++)
            zeros.push(0);
        
        arr = zeros.concat(arr);
        return arr;
    }
    //slides the whole matrix a.k.a all columns to the right direction
    matrixSlide(){
        for (let i = 0; i < this.size; i++)
            this.matrix[i] = this.move(this.matrix[i]);
    }
    //calls 3 methods to perform a slide, combine, slide task
    //which simulates the complete slide of column: takes 1 parameter
    //param->col: given column of the matrix
    move(col){
        col = this.slide(col)
        col = this.combine(col)
        col = this.slide(col)
        return col;
    }
    //moves(see: method move) to the specific direction: takes 1 parameter
    //direction: the direction to move
    moveToDirection(direction){
        console.clear();
        const prevMatrix = Helper.copyMatrix(this.matrix);

        let isReversed = false;
        let isRotated = false;
        switch(direction){
            case directions.top:
                this.rotate();
                isRotated = true;
                this.reverse();
                isReversed = true;
                break;
            case directions.left:
                this.reverse();
                isReversed = true;
                break;
            case directions.bottom:
                this.rotate();
                isRotated = true;
                break; 
            case directions.right:
                break;
        }

        this.matrixSlide();
        
        if (isReversed)
            this.reverse();
        if (isRotated)
            for (let i = 0; i < 3; i++)
                this.rotate();
        
        if (prevMatrix !== this.matrix)
            this.generate();

        console.table(this.matrix);
        this.graphic.drawGrid();
    }
    //combines all possible values of current column: takes 1 parameter
    //param->col: given column of the matrix
    combine(col){
        for (let i = this.size - 1; i >= 1; i--)
            if (col[i] === col[i - 1]){
                col[i] *= 2;
                col[i - 1] = 0;            
            }

        return col;
    }
    //wrapper method of Helper.reverseMatrix().
    reverse(){
        Helper.reverseMatrix(this.matrix);        
    }
    //wrapper method of Helper.rotateMatrix().
    rotate(){
        Helper.rotateMatrix(this.matrix);
    }
}
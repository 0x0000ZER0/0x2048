//definitons of Cell class. Is used to instantiate new cells
class Cell{
    //custom constructor: takes 2 parameters
    //param->col: the cells column
    //param->row: the cells row
    constructor(col, row){
        this.col = col;
        this.row = row;
    }
}
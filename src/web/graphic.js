//defintion of Graphic class. Is used to draw the matrix.
class Graphic{
    //custom constructor: takes 1 parameter
    //param->game: The reference of the game object
    constructor(game){
        this.matrix = game.matrix;
        this.cellSize = 100;
        this.size = game.size; 
        this.container = document.getElementById('container');
    }
    //draws the matrix in form of grid
    drawGrid(){
        this.clearGrid();
        const width = this.size * this.cellSize;
        const height = width;

        $('#container').css({
            'position': 'absolute',
            'width': `${width}px`,
            'height': `${height}px`
        });

        this.drawCells();
    }
    //loops through the matrix to draw the cells
    drawCells(){
        for (let c = 0; c < this.size; c++)
            for (let r = 0; r < this.size; r++)
                this.drawCell(r, c, this.matrix[c][r]);
    }
    //draws a single cell: takes 3 parameters
    //param->c: The column of current cell
    //param->r: The row of current cell
    //param->text: The value of current cell
    drawCell(c, r, text){
        const value = text <= 0 ? '' : text;
        const valueColor = text > 0 && text <= 2 ? '#dcf713' :
                           text > 2 && text <= 4 ? '#dd940d' :
                           text > 4 && text <= 8 ? '#f94d09' :
                           text > 8 && text <= 16 ? '#f90909' :
                           text > 16 && text <= 32 ? '#99f909' :
                           text > 32 && text <= 64 ? '#2df909' :
                           text > 64 && text <= 128 ? '#108243' :
                           text > 128 && text <= 256 ? '#08d8d8' :
                           text > 256 && text <= 1024 ? '#d508d8' :
                           text > 1024 && text <= 2048 ? '#d80888' :
                           text > 2048 ? '#ffffff' : '#ffffff' ;  

        this.container.innerHTML += `<div id='cell-c${c}-r${r}'>${value}<div>`;
        $(`#cell-c${c}-r${r}`).css({
            'position': 'absolute',
            'left': `${c * this.cellSize}px`,
            'top': `${r * this.cellSize}px`,
            'width': `${this.cellSize}px`,
            'height': `${this.cellSize}px`,
            'border': `2px #ffffff solid`,
            'font-size': '30px',
            'display': 'flex',
            'justify-content': 'center',
            'align-items': 'center',
            'color': `${valueColor}`,
            'background-color': `#262626`
        });
    }
    //clears the current grid/matrix
    clearGrid(){
        this.container.innerHTML = ''; 
    }
}
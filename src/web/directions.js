//defines a enum for all different directions. NOTE: the values can NOT be combined with binary operators.
const directions = {
    top: 1,
    left: 2,
    bottom: 3,
    right: 4
}
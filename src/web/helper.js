//definition of Helper class. Is used to create extension methods
class Helper{
    //default constructor
    constructor(){

    }
    //returns a random nubmer: takes 2 parameters
    //param->start: defines the start endpoint
    //param->end: defines the end endpoint 
    static getRandomNumber(start, end){
        end = end - start + 1;
        return Math.floor(Math.random() * end) + start;
    }
    //returns a new matrix based on size: takes 2 parameters
    //param->size: defines the size of the new matrix
    //param->value: sets the value of the new matrix
    static newMatrix(size = 4, value = 0){
        let matrix = this.newArray(size);

        for (let i = 0; i < size; i++)
            matrix[i] = this.newArray(size);

        for (let c = 0; c < size; c++)
            for (let r = 0; r < size; r++)
                matrix[c][r] = value;

        return matrix;
    }
    //returns a new array: takes 1 parameter
    //param->size: defines the size of the new array
    static newArray(size){
        return new Array(size);
    }
    //returns a new copy of the given matrix: takes 1 parameter
    //param->matrix: the matrix to copy
    static copyMatrix(matrix){
        let copy = this.newMatrix(matrix.length);
        
        for (let c = 0; c < copy.length; c++)
            for (let r = 0; r < copy[c].length; r++)
                copy[c][r] = matrix[c][r];

        return copy;
    }
    //returns a new reversed array: takes 1 parameter
    //param->arr: the array to reverse
    static reverseArray(arr){
        let rev = []
        for (let i = arr.length - 1; i >= 0; i--)
            rev.push(arr[i]);

        return rev;
    }
    //reverses the current matrix: takes 1 parameter
    //param->matrix: matrix to reverse
    static reverseMatrix(matrix){
        for (let i = 0; i < matrix.length; i++)
            matrix[i] = this.reverseArray(matrix[i]);
    }
    //returns the rotated copy of given matrix: takes 1 parameter
    //param->matrix: matrix to rotate
    static rotateMatrix(matrix){
        let copy = this.newMatrix(matrix.length);

        for (let c = 0; c < copy.length; c++)
            for (let r = 0; r < copy[c].length; r++)
                copy[c][r] = matrix[r][c];
        
        //setting the value of matrix manually instead of returning it, because JS does have pointer/reference issues!!! 
        for (let c = 0; c < copy.length; c++)
            for (let r = 0; r < copy[c].length; r++)
                matrix[c][r] = copy[c][r];
    }
}